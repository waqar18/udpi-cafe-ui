import { ApplicationSettings } from "./applicationSettiings";
import { Category } from "./category";
import { Menu } from "./menu";

export class CategoryAndMenuAndAppSettings {
    Category: Array<Category>;
    Menu: Array<Menu>;
    AppSettings: Array<ApplicationSettings>;
}