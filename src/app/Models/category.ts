import { Menu } from "./menu";

export class Category {
    CategoryID: number;
    CategoryName:string;
    Discount:number;
    ImageURL:string;
    Notes:string;
    RestaurantID:number;
    base64Url: string;
    Menu: Array<Menu>;
    }