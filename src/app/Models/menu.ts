export class Menu {
    // [x: string]: any;
    // forEach(arg0: (o: any) => void) {
    //   throw new Error('Method not implemented.');
    // }
    CategoryID: number;
    MenuID: number;
    MenuName: string;
    Discount: number;
    ImageURL: string;
    base64Url: string;
    Notes: string;
    RestaurantID: number;
    LocationID: number;
    OriginalPrice: number;
    PreparationTime: number;
    Price: number;
    Quantity: number;
    SortOrder: number;
    StatusID: number;
    UnitID: number;
    WillDeliver: boolean;
    count: number;
    num: any;
    NewPrice: number;
    Selected: boolean = false;
    }