import { Time } from "@angular/common";
import { Menu } from "./menu";
import { orderItem } from "./orderitem";

export class Order {
    OrderID
    RestaurantID: number;
    LocationID:number;
    VenuTableID:number;
    OrderOn:Date;
    TotalAmount:number;
    StaffID:number;
    CustomerID:number;
    CustomerFirstName:string;
    CustomerLastName:string;
    MobilePhone:string;
    email:string;
    Quantity:string
    PreparationTime:number;
    DeliveryTime:number;
    StatusID:number;
    Rating:number;
    Feedback:string;
    PromoCode:string;
    DeliveryServiceID:number;
    OrderTypeID:number;
    DeliveryAddress:string;
    DeliveryServicePhone:string;
    Gratuity:number;
    Tip:number;
    orderItem:orderItem[];
    menu:Menu[];
  order: Order[];
    }

