import { Menu } from "./menu";

export class orderItem {
    OrderItemID:number
    OrderID: number;
    MenuID:number;
    Discount:number;
    Price:number;
    Quantity:any;
    Notes:string;
    Rating:number;
    StatusID:number;
    PreparationTime:number;
    CompletionTime:string;
    Menu:Menu[];
    }

