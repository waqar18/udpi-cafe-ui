export class User {
    id: number;
    name: string;
    email: string;
    password: string;
    phoneNumber: string;
    confirmPassword:string
    token?: string;
    }