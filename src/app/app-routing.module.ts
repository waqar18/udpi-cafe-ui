import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './shared/auth/auth-guard.service';
import { DashboardComponent } from './modules/admin/dashboard/dashboard.component';
import { LandingPagePreventGuard } from './shared/guard/landing-page-prevent.guard';
const appRoutes: Routes = [
  { path: 'Dashboard', component: DashboardComponent
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: 'authentication',
     loadChildren: () => import('./modules/authentication/authentication.module').then(m => m.AuthenticationModule)
  },
  {
    path: '',
    canActivate: [LandingPagePreventGuard],
    loadChildren: () => import('./modules/public/public.module').then(m => m.PublicModule)
  },
  { path: ''
  , redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
