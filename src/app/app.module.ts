import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from "./shared/shared.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { 
    PerfectScrollbarModule, 
    PERFECT_SCROLLBAR_CONFIG, 
    PerfectScrollbarConfigInterface
  } from 'ngx-perfect-scrollbar';
import { AppComponent } from './app.component';
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";
import { AuthService } from './shared/auth/auth.service';
import { AuthGuard } from './shared/auth/auth-guard.service';
import { SimpleNotificationsModule } from 'angular2-notifications';
 import { KitchenComponent } from './modules/kitchen/kitchen.component';
import { CountdownModule } from 'ngx-countdown';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { AdminModule } from './modules/admin/admin.module';
import { AuthenticationModule } from './modules/authentication/authentication.module';





const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true,
    wheelPropagation: false
  };
  
  export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
  }


  @NgModule({
    declarations: [AppComponent, FullLayoutComponent, ContentLayoutComponent,KitchenComponent],
    imports: [
      AppRoutingModule,
      SharedModule,
      HttpClientModule,
      CountdownModule,
      AuthenticationModule,
      NgbModule,
      NgCircleProgressModule.forRoot({
        "backgroundColor": "#FDB900",
        "radius": 100,
        "maxPercent": 100,
        "units": " % Order",
        "unitsColor": "#483500",
        "outerStrokeWidth": 5,
        "outerStrokeColor": "#FFFFFF",
        "innerStrokeColor": "#FFFFFF",
        "titleColor": "#483500",
        "subtitleColor": "#483500",
        "showSubtitle": false,
        "showInnerStroke": false,
        "startFromZero": false
      }),
      BrowserAnimationsModule, // required animations module
      ToastrModule.forRoot({
        timeOut: 5000,
        progressBar: true,
        progressAnimation:"increasing",
      }), // ToastrModule added
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: createTranslateLoader,
          deps: [HttpClient]
        }
      }),
      PerfectScrollbarModule
    ],
    providers: [
      AuthService,
      AuthGuard,
      {
        provide: PERFECT_SCROLLBAR_CONFIG,
        useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
      },
      { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG }
    ],

    bootstrap: [AppComponent]
    
  })
  export class AppModule {}
  