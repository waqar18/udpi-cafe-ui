import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'app/Models/category';
import { CategoryService } from 'app/shared/services/category.service';
import { CustomHttpService } from 'app/shared/services/custom-http.service';
import { SharedService } from 'app/shared/services/shared.service';
import { environment } from 'environments/environment';
import * as _ from 'lodash';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
  baseURL;
  CategoryID = 0;
  category: Category;
  submitted: false;
  selectedFile: File;
  url: string;
  isImageSaved:boolean =false;
  editImage:boolean;
  imageError: string;
  cardImageBase64: string;
  file: File;
  
  

  constructor(private router: Router, private _customService: CustomHttpService, 
    private _sharedService: SharedService,
    private _CategoryService: CategoryService,
     private route: ActivatedRoute,
     public http: HttpClient
     )
      { 
    this.baseURL = environment.API_URL;
    this.category = new Category();
     this.CategoryID = this.route.snapshot.params.id;
  }
  ngOnInit(): void {
    // if(this.CategoryID != 0 ){
    //   this.getCategoryByID();
    // }
    if(this.route.snapshot.params.id !=undefined){
      this.getCategoryByID();
    }

  }
  back(){
    this.router.navigate(['admin/Category'])
  }
  addCategory(category){
    console.log(category)
   if(this.category.base64Url !=null){
    this._customService.postWithoutHeader('api/Category/SaveCategory',category ).subscribe({  
      next: (response: any) => {
 
      this._sharedService.success("Success", "Added successfully");
      this.router.navigate(['admin/Category']);
      },
      error: (error) => {
        this._sharedService.error("error", error.error_description );
      },
      });
   }
   else{
     this._sharedService.toastr.warning('','Add Image',{
       progressBar:false      
     });
   }
  }
  onSelect(event){
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      this.selectedFile = <File>event.target.files[0];
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
      this.url = event.target.result.toString();
       this.category.base64Url = this.url;

    }
  }
}
getCategoryByID(){
  this.http
      .get(
        environment.API_URL +
          '/api/Category/GetCategoryByID/' +
          this.route.snapshot.params.id
      )
      .subscribe((res: any) => {
       
        this.CategoryID = this.route.snapshot.params.id;
        this.category= res;
        this.isImageSaved=false;
      });

}
EditCategory(){
  this._CategoryService.updateCategory(this.category).subscribe(data=>{ 
    this._sharedService.success('success',"updated successfully")
    this.router.navigate(["admin/Category"]);
     });
}

fileChangeEvent(event) {
  this.imageError = null;
  if (event.target.files && event.target.files[0]) {
      // Size Filter Bytes
      // tslint:disable-next-line: variable-name
      // tslint:disable-next-line: variable-name
      const max_size = 20971520;
      // tslint:disable-next-line: variable-name
      const allowed_types = ['image/png', 'image/jpeg'];
      // tslint:disable-next-line: variable-name
      const max_height = 15200;
      // tslint:disable-next-line: variable-name
      const max_width = 25600;

      if (event.target.files[0].size > max_size) {
          this.imageError =
              'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          return false;
      }

      if (!_.includes(allowed_types, event.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          return false;
      }
      // tslint:disable-next-line: no-shadowed-variable
      const reader = new FileReader();
      reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
              // tslint:disable-next-line: variable-name
              const img_height = rs.currentTarget['height'];
              // tslint:disable-next-line: variable-name
              const img_width = rs.currentTarget['width'];

              console.log(img_height, img_width);


              if (img_height > max_height && img_width > max_width) {
                  this.imageError =
                      'Maximum dimentions allowed ' +
                      max_height +
                      '*' +
                      max_width +
                      'px';
                  return false;
              } else {
                  const imgBase64Path = e.target.result;
                  this.cardImageBase64 = imgBase64Path;
                  this.category.base64Url = imgBase64Path;
                  this.isImageSaved = true;
                  // this.previewImagePath = imgBase64Path;
              }
          };
      };

      reader.readAsDataURL(event.target.files[0]);
  }
}
removeImage() {
  this.isImageSaved = false;
  }
  changeImage(){
    
    // this.isImageSaved = true;
 
  }
}