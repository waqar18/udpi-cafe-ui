import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Menu } from 'app/Models/menu';
import { CategoryService } from 'app/shared/services/category.service';
import { CustomHttpService } from 'app/shared/services/custom-http.service';
import { SharedService } from 'app/shared/services/shared.service';
import { environment } from 'environments/environment';
import * as _ from 'lodash';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss'],
})
export class AddItemComponent implements OnInit {
  menu: Menu;
  MenuList: any;
  MenuID:number;
  category: any;
  url: string;
  MenuItemList:any
  selectedCategory: any;
  baseUrl = environment.API_URL;
  isImageSaved:boolean;
  imageError: string;
  cardImageBase64: string;
  selectedFile: File;
  constructor(
    private _CategoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute,
    private _customService: CustomHttpService,
    public _sharedService: SharedService,
    private Category: CategoryService,
    public http: HttpClient
  ) {
    this.menu = new Menu();
    if (
      this._sharedService.updateMenu != undefined ||
      this._sharedService.updateMenu != null
    ) {
      this.menu = this._sharedService.updateMenu;
      // this.menu.ImageURL = null;
      console.log('menu', this.menu)
    }
    this.baseUrl = environment.API_URL;
  }

  ngOnInit(): void {
    this.Category.GetCategory().subscribe((data) => {
      this.category = data;
    });

    if(this.route.snapshot.params.id !=undefined){
      this.getMenuItem();
    }

    
  }
  addMenu() {
    
    console.log(this.menu)
    // if( this.route.snapshot.params.id !=null){
      if(this.menu.base64Url !=null){
    this._customService
      .postWithoutHeader('api/Category/SaveMenu', this.menu)
      .subscribe({
        next: (response: any) => {
          this._sharedService.success('Success', 'Added successfully');
          this.router.navigate(['admin/Menu-item']);
        },
        error: (error) => {
          this._sharedService.error('error', error.error_description);
        },
      });
  }
  else{
    this._sharedService.toastr.warning('','Add Image',{
      progressBar:false
    });
  }
}
  editMenu() {
    console.log('this.menu', this.menu);
    this._CategoryService.updateMenu(this.menu).subscribe((data) => {
      console.log('respones', data);
      this._sharedService.success('Success', 'Added successfully');
      this.router.navigate(['admin/Menu-item']);
    });
  }
  onSelect(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      this.selectedFile = <File>event.target.files[0];
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.url = event.target.result.toString();
        this.menu.base64Url = this.url;
        this.url = null;
      };
    }
  }
  back() {
    this.router.navigate(['admin/Menu-item']);
  }
  changeCategory($event) {
    // this.menu.CategoryID = $event;
  }
  changeSmsService($event) {
    this.menu.CategoryID = $event;
  }

getMenuItem(){
  this.http
      .get(
        environment.API_URL +
          '/api/Category/GetMenuByID/' +
          this.route.snapshot.params.id
      )
      .subscribe((res: any) => {
        this.MenuID = this.route.snapshot.params.id;
        this.menu = res;
        this.isImageSaved = false;
      });

}

fileChangeEvent(event) {
  this.imageError = null;
  if (event.target.files && event.target.files[0]) {
      // Size Filter Bytes
      // tslint:disable-next-line: variable-name
      // tslint:disable-next-line: variable-name
      const max_size = 20971520;
      // tslint:disable-next-line: variable-name
      const allowed_types = ['image/png', 'image/jpeg'];
      // tslint:disable-next-line: variable-name
      const max_height = 15200;
      // tslint:disable-next-line: variable-name
      const max_width = 25600;

      if (event.target.files[0].size > max_size) {
          this.imageError =
              'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          return false;
      }

      if (!_.includes(allowed_types, event.target.files[0].type)) {
          this.imageError = 'Only Images are allowed ( JPG | PNG )';
          return false;
      }
      // tslint:disable-next-line: no-shadowed-variable
      const reader = new FileReader();
      reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
              // tslint:disable-next-line: variable-name
              const img_height = rs.currentTarget['height'];
              // tslint:disable-next-line: variable-name
              const img_width = rs.currentTarget['width'];

              console.log(img_height, img_width);


              if (img_height > max_height && img_width > max_width) {
                  this.imageError =
                      'Maximum dimentions allowed ' +
                      max_height +
                      '*' +
                      max_width +
                      'px';
                  return false;
              } else {
                  const imgBase64Path = e.target.result;
                  this.cardImageBase64 = imgBase64Path;
                  this.menu.base64Url = imgBase64Path;
                  this.isImageSaved = true;
              }
          };
      };

      reader.readAsDataURL(event.target.files[0]);
  }
}
removeImage() {
  this.isImageSaved = false;
  }
}
