import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoggedInGuard } from '../authentication/logged-in.guard';
import { KitchenComponent } from '../kitchen/kitchen.component';

import { AddCategoryComponent } from './add-category/add-category.component';
import { AddItemComponent } from './add-item/add-item.component';
import { AdminComponent } from './admin/admin.component';
import { CategoryComponent } from './category/category.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { KitchenScreenGuard } from './kitchen-screen.guard';
import { KitchenscreenComponent } from './kitchenscreen/kitchenscreen.component';
import { MenuitemComponent } from './menuitem/menuitem.component';
import { OrderComponent } from './order/order.component';
import { PriceUpdateComponent } from './price-update/price-update.component';
import { RoleGuardService } from './role-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'kitchen-screen',
        component: KitchenComponent,
        canActivate: [RoleGuardService],
        data: { roles:  [2]}
      },
      {
        path: 'order-status',
        component: KitchenscreenComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
      },
      {
        path: 'DashBoard',
        component: DashboardComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
      },
      {
        path: 'Category',
        component: CategoryComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
       
      },
      {
        path: 'Menu-item',
        component: MenuitemComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
      },
      {
        path: 'add-Menu',
        component: AddItemComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
      },
      {
        path: 'edit-Menu/:id',
        component: AddItemComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
      },
      {
        path: 'add-category',
        component: AddCategoryComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
      },
      {
        path: 'edit-Category/:id',
        component: AddCategoryComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
      },
      {
        path: 'order',
        component: OrderComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
      },
      { path: 'priceUpdate', 
        component: PriceUpdateComponent,
        canActivate: [RoleGuardService],
        data: { roles: [1]}
      },
      // { path: '', redirectTo: 'Category', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
