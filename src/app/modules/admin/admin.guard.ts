import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';


@Injectable()
export class AdminService implements CanActivate {
  constructor(private router: Router) {}
  canActivate(): boolean {
    const currentUser = localStorage.getItem('name');
    const userRole = localStorage.getItem('RoleID');
    // tslint:disable-next-line: prefer-const
    // tslint:disable-next-line: radix
    const roleId = parseInt(userRole);
    if (currentUser && (roleId === 1)) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
    this.router.navigate(['/login']);
    return false;
      }
}
