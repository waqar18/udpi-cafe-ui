import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KitchenscreenComponent } from './kitchenscreen/kitchenscreen.component';
import { SharedModule } from 'app/shared/shared.module';
import { CategoryComponent } from './category/category.component';
import { MenuitemComponent } from './menuitem/menuitem.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AddCategoryComponent } from './add-category/add-category.component';
import { AddItemComponent } from './add-item/add-item.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { CountdownModule } from 'ngx-countdown';
import { OrderComponent } from './order/order.component';
import { OrderItemComponent } from './order-item/order-item.component';
import { InputTypeFileDirective } from './input-type-file.directive';
import { PriceUpdateComponent } from './price-update/price-update.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { UserRolesService } from './user-roles.service';
import { RoleGuardService } from './role-guard.service';

@NgModule({
  declarations: [AdminComponent,
             DashboardComponent, KitchenscreenComponent, CategoryComponent,
             MenuitemComponent, AddCategoryComponent, AddItemComponent, EditCategoryComponent
             , OrderComponent, OrderItemComponent, InputTypeFileDirective, PriceUpdateComponent ],
  imports: [
    CommonModule,
    AdminRoutingModule,
   NgxDatatableModule,
   CountdownModule,
   SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  providers:[
    DecimalPipe,
    UserRolesService,
     RoleGuardService
  ]
})
export class AdminModule { }
