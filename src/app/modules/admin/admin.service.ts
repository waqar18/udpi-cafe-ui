import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';
import { CustomHttpService } from 'app/shared/services/custom-http.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  baseURL =  environment.API_URL;
  constructor(private _customHttpService: CustomHttpService) { }
  getAllCategoriesAndMenu() {
    return this._customHttpService.get('api/Category/GetAllCategoryAndMenu');
  }
  massUpdateMenu(item: any) {
    return this._customHttpService.post('api/Category/MassUpdateMenuPrice', item);
  }
  perItemPriceUpdateMenu(item: any) {
    return this._customHttpService.post('api/Category/MenuPerItemPriceUpdate', item);
  }
  UpdateTax(item: any) {
    return this._customHttpService.post('api/Category/UpdateApplicationSetings', item);
  }
}
