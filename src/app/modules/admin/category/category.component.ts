import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { CategoryService } from "app/shared/services/category.service";
import { CustomHttpService } from "app/shared/services/custom-http.service";
import { SharedService } from "app/shared/services/shared.service";
import { environment } from "environments/environment";
declare var require: any;
const data: any = require("../../../shared/data/company.json");
@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.scss"],
})
export class CategoryComponent implements OnInit {
  category: any;
  baseURL;
  id:number;
  baseUrl = environment.API_URL;
  constructor(
    public router: Router,
    private _CategoryService: CategoryService,
    public http: HttpClient,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    public _sharedService: SharedService
  ) {
    this.baseURL = environment.API_URL;
  }

  ngOnInit(): void {
   this.getCategory();
  }
  addCategory() {
    this.router.navigate(["admin/add-category"]);
  }
  deleteCategory1() {
    this.modalService.dismissAll();
    this._CategoryService.deleteCategory(this.id).subscribe((res) => {
      // this.category = res;
       this.getCategory();
      this._sharedService.success("success", "Deleted successfully");
    });
  }
  getCategory() {
    this._CategoryService.GetCategory().subscribe((data) => {
      this.category = data;
    });
  }
  // updateCategory(item) {
  //   this._sharedService.updateCategory = item;
  //   this.router.navigate(["admin/edit-category"]);
  // }
  updateCategory(CategoryID:number) {
    this.router.navigate(['admin/edit-Category/' + CategoryID]);
  }
  deleteModel(CategoryID){
     this.id = CategoryID;
   }
   openModal(CategoryID,customContent) {
    this.id = CategoryID;
     console.log('this.categoryid',CategoryID)
    this.modalService.open(customContent, { windowClass: 'dark-modal' });
}
}
