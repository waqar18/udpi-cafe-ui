import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'app/Models/category';
import { CategoryService } from 'app/shared/services/category.service';
import { CustomHttpService } from 'app/shared/services/custom-http.service';
import { SharedService } from 'app/shared/services/shared.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {
  baseURL;
  category:Category;
  submitted:false;
  selectedFile: File;
  url: string;

  constructor(private router:Router,
     private _customService:CustomHttpService,private _CategoryService:CategoryService, public _sharedService: SharedService, private route: ActivatedRoute,) { 
    this.baseURL = environment.API_URL;
    this.category = new Category();
    this.category = this._sharedService.updateCategory;
  }

  ngOnInit(): void {
  }

//   back(){
//     this.router.navigate(['admin/Category'])
//   }
//   updateCategory(){
//      this._CategoryService.updateCategory(this.category).subscribe(data=>{
//       this.category = this.category;  
//       this._sharedService.success('success',"updated successfully")
//       this.router.navigate(["admin/Category"]);
//        });

//      }
//   onSelect(event){
//     if (event.target.files && event.target.files[0]) {
//       var reader = new FileReader();
//       this.selectedFile = <File>event.target.files[0];
//       reader.readAsDataURL(event.target.files[0]);
//       reader.onload = (event) => {
//       this.url = event.target.result.toString();
//        this.category.base64Url = this.url;
//     }
//   }
// }
}
