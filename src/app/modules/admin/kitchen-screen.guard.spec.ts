import { TestBed } from '@angular/core/testing';

import { KitchenScreenGuard } from './kitchen-screen.guard';

describe('KitchenScreenGuard', () => {
  let guard: KitchenScreenGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(KitchenScreenGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
