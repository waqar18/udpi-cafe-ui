import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KitchenscreenComponent } from './kitchenscreen.component';

describe('KitchenscreenComponent', () => {
  let component: KitchenscreenComponent;
  let fixture: ComponentFixture<KitchenscreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KitchenscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KitchenscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
