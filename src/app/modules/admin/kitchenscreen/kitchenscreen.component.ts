import { HttpClient } from '@angular/common/http';
import { AfterContentChecked, AfterViewChecked, AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Menu } from 'app/Models/menu';
import { Order } from 'app/Models/order';
import { orderItem } from 'app/Models/orderitem';
import { kOrders } from 'app/Models/orders';
import { SharedService } from 'app/shared/services/shared.service';
import { environment } from 'environments/environment';
import { CountdownComponent } from 'ngx-countdown';
import { number } from 'ngx-custom-validators/src/app/number/validator';


@Component({
  selector: 'app-kitchenscreen',
  templateUrl: './kitchenscreen.component.html',
  styleUrls: ['./kitchenscreen.component.scss']
})
export class KitchenscreenComponent implements OnInit,AfterViewChecked {
  isShown: boolean = false ; // hidden by default
  OrederDetail:any=[];
  customerid;
  cartItemStatus=Array<orderItem>();
   AddToList=Array<Order>();
   menulist=Array<Menu>();
  kitchenDetailorder:kOrders;
  time;
  orderitem:orderItem
  preparation= 0;
  preparation2 :any;
  currentTime: any;
  targetTime: any;
  interval: any;
  baseURL;
  selectedRowIndex;
  config: any; 

  isDataLoaded = false;
  constructor(private router:Router, public http: HttpClient,public shardService: SharedService) { 
   this.kitchenDetailorder = new kOrders();
   this.orderitem= new orderItem(); 
    this.baseURL = environment.API_URL;
  }

  ngOnInit(): void {
    this.orderDetailAll();
  }

  ngAfterViewChecked()
  {
    if(this.isDataLoaded == true)
    {
      this.cartItemStatus.forEach(element => {
        if(element.StatusID != 3)
        {
          var dataElement = document.getElementById(element.OrderItemID.toString());
          if(dataElement != null)
          {
            dataElement.removeAttribute('hidden');
          }
        }
      });
    }
  }

orderDetailAll(){
  this.http
    .get(
      environment.API_URL +
        "api/Category/GetAllOrders").subscribe((res: any) => {
        this.kitchenDetailorder= res;
       
        this.AddToList =this.kitchenDetailorder.order;
       
        this.cartItemStatus=this.kitchenDetailorder.orderItem;
        this.isDataLoaded = true;
        })
}
      completeStatus(id: number,$CountDown){
      this.orderitem.OrderItemID=id;
      this.orderitem.StatusID =3;
      $CountDown.stop();
         this.http.post(environment.API_URL+"api/Category/UpdateOrderItem",this.orderitem).subscribe((res:any)=>{
           this.shardService.success("Success","Updated successfully");
           this.orderDetailAll();
         })
      }
     
    }

