import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'app/shared/services/category.service';
import { SharedService } from 'app/shared/services/shared.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-menuitem',
  templateUrl: './menuitem.component.html',
  styleUrls: ['./menuitem.component.scss']
})
export class MenuitemComponent implements OnInit {
  menu:any
  id:number;
  menuList:any;
  baseURL
  constructor(public router:Router,private Menu:CategoryService,private _CategoryService:CategoryService,public http: HttpClient, private modalService: NgbModal,
    private route: ActivatedRoute, public _sharedService:SharedService) { 
      this.baseURL = environment.API_URL
  }

  ngOnInit(): void {
    this.Menu.GetAllMenu().subscribe(data=>{
      this.menu =data;
     
    })
  }
 
  addMenu(){
    this.router.navigate(["admin/add-Menu"]);
    this._sharedService.updateMenu =null;
  }
  deleteMenu(){
    this.modalService.dismissAll();
    this._CategoryService.deleteMenu(this.id).subscribe(res=>{
      // this.menu = data;
      this.ngOnInit();
      this._sharedService.success('success',"Deleted successfully")
    })
}
// updateMenu(item){
//    this._sharedService.updateMenu = item;
//    this.router.navigate(["admin/add-Menu"]);
// }
updateMenu(MenuID: number){
  this.router.navigate(['admin/edit-Menu/' + MenuID]);
  console.log('Menu ID',MenuID)
}
openModal(MenuID,customContent) {
  this.id = MenuID;
   console.log('this.categoryid',MenuID)
  this.modalService.open(customContent, { windowClass: 'dark-modal' });
}
}
