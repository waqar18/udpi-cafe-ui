import { environment } from './../../../../environments/environment';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.scss']
})
export class OrderItemComponent implements OnInit {
  @Input() orderItem: any;
  @Input() orderID: any;
  @Input() gratuity: any;
  @Input() tip: any;
  @Input() totalAmount: any;
  base_URL = environment.API_URL;
  length = 0;
  @Output() Orderlength = new EventEmitter<number>();
  constructor() { }

  ngOnInit(): void {
  }
  hideSideBar() {
    this.Orderlength.emit(this.length);
  }
}
