import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { orderItem } from 'app/Models/orderitem';
import { kOrders } from 'app/Models/orders';
import { SharedService } from 'app/shared/services/shared.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {
  orderList: any;
  orderItemList: any;
  orderitem: any;
  baseURL: any;
  AddToList: any;
  cartItemStatus: any;
  filtered: any;
  orderLength = 0;
  OrderID: any;
  Gratuity: any;
  Tip: any;
  TotalAmount: any;
  constructor(
    private router: Router,
    public http: HttpClient,
    public shardService: SharedService
  ) {
    this.orderList = new kOrders();
    this.orderitem = new orderItem();
    this.baseURL = environment.API_URL;
  }

  ngOnInit(): void {
    this.orderDetailAll();
  }

  orderDetailAll() {
    this.http
      .get(environment.API_URL + 'api/Category/GetAllOrdersWithAllStatus')
      .subscribe((res: any) => {
        this.orderList = res;
        this.AddToList = this.orderList.order;
        this.orderItemList = this.orderList.orderItem;
        this.cartItemStatus = this.orderList.orderItem;
      });
  }
  orderItemAgainstOrderID(orderID: any, Gratuity: any, tip: any, TotalAmount: any) {
    this.filtered = this.orderItemList.filter(orders => orders.OrderID === orderID);
    this.orderLength = orderID;
    this.Gratuity = Gratuity;
    this.Tip = tip;
    this.OrderID = orderID;
    this.TotalAmount = TotalAmount;
  }
  scroll() {
    var el = document.getElementById("order");
    el.scrollIntoView({
      behavior: "smooth"
    });
  }
  receiveLength($event) {
    this.orderLength = $event;
  }
}
