import { ApplicationSettings } from "./../../../Models/applicationSettiings";
import { environment } from "./../../../../environments/environment";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Category } from "app/Models/category";
import { Menu } from "app/Models/menu";
import { AdminService } from "../admin.service";
import { NgSelectComponent, NgSelectConfig } from "@ng-select/ng-select";
import { SharedService } from "app/shared/services/shared.service";
import { DecimalPipe } from "@angular/common";

@Component({
  selector: "app-price-update",
  templateUrl: "./price-update.component.html",
  styleUrls: ["./price-update.component.scss"],
})
export class PriceUpdateComponent implements OnInit {
  category: Array<Category>;
  menu: Array<Menu>;
  filtered: Array<Menu>;
  selectedCategory: any;
  checkedMenu: Array<Menu>;
  applicationSettings: Array<ApplicationSettings>;
  isEdit = false;
  baseURL = environment.API_URL;
  psAmount: number;
  isSelected = false;
  @ViewChild(NgSelectComponent) ngSelectComponent: NgSelectComponent;
  @ViewChild("abcde") abcde: ElementRef;
  constructor(
    private _adminService: AdminService,
    private config: NgSelectConfig,
    public _sharedService: SharedService,
    private _decimalPipe: DecimalPipe
  ) {
    this.category = new Array<Category>();
    this.menu = new Array<Menu>();
    this.checkedMenu = new Array<Menu>();
    this.filtered = new Array<Menu>();
    this.applicationSettings = new Array<ApplicationSettings>();
    this.config.notFoundText = "Custom not found";
    this.config.appendTo = "body";
    this.config.bindValue = "value";
  }

  ngOnInit(): void {
    this.GetAllCategoriesAndMenu();
    console.log(this.filtered);
  }
  GetAllCategoriesAndMenu() {
    this._adminService.getAllCategoriesAndMenu().subscribe({
      next: (response: any) => {
        this.category = response.Categories;
        this.menu = response.Menus;
        this.applicationSettings = response.ApplicationSettings;
        console.log("category", this.category);
        console.log("menu", this.menu);
        console.log("applicationSettings", this.applicationSettings);

        for (let i = 0; i < this.menu.length; i++) {
          this.menu[i].Selected = false;
          this.menu[i].NewPrice = this.menu[i].Price;
        }
      },
      error: (error) => {
        console.log("error", error);
      },
    });
  }
  filterMenu(CategoryID) {
    if (CategoryID != null) {
      var id = parseInt(CategoryID.CategoryID);
      this.filtered = this.menu.filter(
        (category) => category.CategoryID === id
      );
      for (let i = 0; i < this.menu.length; i++) {
        this.menu[i].Selected = false;
        this.menu[i].NewPrice = this.menu[i].Price;
      }
      console.log("Filtered Menu", this.filtered);
    } else {
      this.filtered = new Array<Menu>();
    }
  }
  decrement(item: number) {
    if (this.isEdit == false) {
      if (this.filtered.length != 0) {
        if (this.filtered.find((el) => el.Selected == true)) {
          for (let i = 0; i < this.filtered.length; i++) {
            if (this.filtered[i].Selected == true) {
              this.psAmount = (this.filtered[i].NewPrice / 100) * item;
              var a = +Number(
                this.filtered[i].NewPrice - this.psAmount
              ).toPrecision(3);
              this.filtered[i].NewPrice = a;
            } 
          }
        }
        else {
          this._sharedService.toastr.warning("", "Select Menu", {
            progressBar: false,
            timeOut: 1000,
          });
        }
      } else {
        this._sharedService.toastr.warning("", "Select Category", {
          progressBar: false,
          timeOut: 1000,
        });
      }
    }
  }
  increment(item) {
    if (this.isEdit == false) {
      if (this.filtered.length != 0) {
        if (this.filtered.find((el) => el.Selected == true)) {
          for (let i = 0; i < this.filtered.length; i++) {
            if (this.filtered[i].Selected == true) {
              this.psAmount = (this.filtered[i].NewPrice / 100) * item;
            var a = +Number(
              this.filtered[i].NewPrice + this.psAmount
            ).toPrecision(3);
            this.filtered[i].NewPrice = a;
            console.log(this.filtered);
            }
          }
        } else {
          this._sharedService.toastr.warning("", "Select Menu", {
            progressBar: false,
            timeOut: 1000,
          });
        }
      } else {
        this._sharedService.toastr.warning("", "Select Category", {
          progressBar: false,
          timeOut: 1000,
        });
      }
    }
  }
  onSelect(index) {
    for (let i = 0; i < this.filtered.length; i++) {
      if (i == index && this.filtered[i].Selected == false) {
        this.filtered[i].Selected = true;
        this.filtered[i].NewPrice = this.filtered[i].Price;
      } else if (i == index && this.filtered[i].Selected == true) {
        this.filtered[i].Selected = false;
        this.filtered[i].NewPrice = this.filtered[i].Price;
      }
    }
  }
  allSelect(event) {
    const checked = event.target.checked;
    this.filtered.forEach((item) => (item.Selected = checked));
    for (let i = 0; i < this.filtered.length; i++) {
      this.filtered[i].NewPrice = this.filtered[i].Price;
    }
  }
  isEditable(event) {
    this.isEdit = true;
    for (let i = 0; i < this.filtered.length; i++) {
      this.filtered[i].Selected = false;
    }
    this.isSelected = false;
  }
  pricePerItemUpdate(item) {
    console.log("item", item);
    if (item.NewPrice != null) {
      this.isEdit = false;
      this._adminService.perItemPriceUpdateMenu(item).subscribe({
        next: (response: any) => {
          console.log("response", response);
          this._sharedService.toastr.success("", "Price Update", {
            progressBar: false,
            timeOut: 1000,
          });
          this.isSelected = false;
          for (let i = 0; i < this.menu.length; i++) {
            this.menu[i].Price = this.menu[i].NewPrice;
          }
        },
        error: (error) => {
          console.log("error", error);
          this._sharedService.toastr.error("", error, {
            progressBar: false,
            timeOut: 1000,
          });
        },
      });
    } else {
      console.log("item null");
    }
  }
  massUpdate() {
    if (this.filtered.length != 0) {
      var Obj = {
        CategoryID: 0,
        Menu: [],
      };
      this.checkedMenu = new Array<Menu>();
      this.filtered.forEach((menu: any) => {
        if (menu.Selected == true) {
          this.checkedMenu.push(menu);
        }
      });
      this.checkedMenu.forEach((menu: any) => {
        Obj.Menu.push(menu);
        Obj.CategoryID = menu.CategoryID;
      });
      console.log("this.checkedMenu", Obj);
      if (this.checkedMenu.length != 0) {
        this._adminService.massUpdateMenu(Obj).subscribe({
          next: (response: any) => {
            console.log("response", response);
            this._sharedService.toastr.success("", "Update Successfully", {
              progressBar: false,
              timeOut: 1000,
            });
            for (let i = 0; i < this.menu.length; i++) {
              this.menu[i].Selected = false;
            }
            this.isSelected = false;
            for (let i = 0; i < this.menu.length; i++) {
              this.menu[i].Price = this.menu[i].NewPrice;
            }
          },
          error: (error) => {
            console.log("error", error);
          },
        });
      } else {
        this._sharedService.toastr.warning("", "Select Menu", {
          progressBar: false,
          timeOut: 1000,
        });
      }
    } else {
      this._sharedService.toastr.warning("", "Select Category", {
        progressBar: false,
        timeOut: 1000,
      });
    }
  }
  UpdateTax(item) {
    console.log("item", item);

    this._adminService.UpdateTax(item).subscribe({
      next: (response: any) => {
        console.log("response", response);
        this._sharedService.toastr.success(
          "",
          "Sales Tax Update Successfully",
          {
            progressBar: false,
            timeOut: 1000,
          }
        );
        for (let i = 0; i < this.menu.length; i++) {
          this.menu[i].Selected = false;
        }
      },
      error: (error) => {
        console.log("error", error);
      },
    });
  }
  closeField() {
    this.isEdit = false;
  }
}
