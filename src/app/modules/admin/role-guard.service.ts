import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { UserRolesService } from './user-roles.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {

  constructor(private getUserRoles: UserRolesService,
    private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    
    var role_id = parseInt(localStorage.getItem('RoleID'))

       if( route.data.roles.find(item => item == role_id) != undefined)
       {
         return true
       }
       else
       {
         if(role_id == 2)
         {
            this.router.navigate(['admin/kitchen-screen']);
         }
         else if(role_id == 1){
          this.router.navigate(['admin/Category']);
        }
        else{
          this.router.navigate(['/'])
        }
       }
  
  }
}
