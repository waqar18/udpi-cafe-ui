import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserRolesService  {

  userRoles: string;
  UserRole:string
  constructor() { }
  setRoles(Roles: string){
    this.UserRole = localStorage.getItem('RoleID');
    this.userRoles =Roles.slice(0);
  }
  getRoles(){
    return this.userRoles;
    
  }
}
