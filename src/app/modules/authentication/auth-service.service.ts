import { Injectable } from '@angular/core';
import { User } from 'app/Models/user';
import { CustomHttpService } from 'app/shared/services/custom-http.service';

@Injectable({
  providedIn: 'root',
})
export class AuthServiceService {
  constructor(private _customService: CustomHttpService) {}
  signin(data?: any) {
    const body =
      'UserName=' +
      data.UserName +
      '&Password=' +
      data.Password +
      '&grant_type=password';
    return this._customService.signin(body);
  }
  register(user?: any) {
    return this._customService.postWithoutHeader('api/Account/Register', user);
  }
}
