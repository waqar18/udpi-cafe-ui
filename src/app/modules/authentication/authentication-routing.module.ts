import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationComponent } from './authentication/authentication.component';
import { LoggedInGuard } from './logged-in.guard';
import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SignUpComponent } from './sign-up/sign-up.component';


const routes: Routes = [{
  path: '',
  component: AuthenticationComponent,
  children: [
    {
      path: 'register', component: SignUpComponent
    },
    {
      path: 'login', component: LoginComponent,
         canActivate: [LoggedInGuard]
    },
    {
      path: 'reset', component: ResetPasswordComponent
    },
    {
      path: '', redirectTo: '/authentication/login', pathMatch: 'full'
    }
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
