import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoggedInGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate(): boolean {
    const currentUser = localStorage.getItem('accesstoken');
    var userRoleId =localStorage.getItem('RoleID');
    if (!currentUser) {
            // logged in so return true
            return true;
        }
        else{
        if(userRoleId == '2'){
          this.router.navigate(['admin/kitchen-screen']);
          return true;
        }  
        else if(userRoleId == '1'){
          this.router.navigate(['admin/Category']);
          return true;
        }
        else{
          this.router.navigate(['/']);
          return true;
        }
        }

        // not logged in so redirect to login page with the return url
    return false;
 }
  
}
