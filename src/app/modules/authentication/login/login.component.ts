import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'app/Models/user';
import { SharedService } from 'app/shared/services/shared.service';
import { AuthServiceService } from '../auth-service.service';
// import * as alertFunctions from '../../../shared/data/sweet-alerts';
import { from } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  Roles: any = [];
  carItem:any;
  customerInfo:any;
  signinForm = new FormGroup({
    UserName: new FormControl('', [Validators.required, Validators.email]),
    Password: new FormControl('', Validators.required),
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: AuthServiceService,
    private _sharedService: SharedService
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    this.service.signin(this.signinForm.value).subscribe({
      next: (response: any) => {
        localStorage.setItem('accesstoken', response.access_token);
        // this.Roles = response.RoleID;
        this._sharedService.menuItems = this.Roles;
        // tslint:disable-next-line: radix
        this._sharedService.selected_role_id = response.RoleID;
        localStorage.setItem('RoleID', response.RoleID);
        localStorage.setItem('name', response.userName);
        this._sharedService.success('', 'Login successfully');
        this._sharedService.logedName = localStorage.getItem('accesstoken');
        // tslint:disable-next-line: radix
        this._sharedService.roleId = parseInt(localStorage.getItem('RoleId'));
        this.carItem = localStorage.removeItem('cart');
        this._sharedService.customer=undefined;
        this.customerInfo=sessionStorage.clear();
      
       

        if(this._sharedService.selected_role_id == 2){
          this.router.navigate(['/admin/kitchen-screen']);  
           
        }
        else if(this._sharedService.selected_role_id == 1){
          this.router.navigate(['/admin/Category']);
        }
        else{
          this.router.navigate(['/']);
        }
        
      },

      error: (error) => {
        this._sharedService.error('error', error.error_description);
      },
    });
  }

  steps() {
    // this.sweetalert.steps();
    this._sharedService.steps();
    // this.router.navigate (['/public/menu-list'])
  }
}
