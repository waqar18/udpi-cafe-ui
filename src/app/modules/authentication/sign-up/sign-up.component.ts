import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'app/Models/user';
import { CustomHttpService } from 'app/shared/services/custom-http.service';
import { SharedService } from 'app/shared/services/shared.service';
import { AuthServiceService } from '../auth-service.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  user: User;
  constructor(private router: Router, private _customService:CustomHttpService,private _sharedService:SharedService,private service:AuthServiceService,
private route: ActivatedRoute,
) { 
    this.user = new User
  }

  ngOnInit(): void {}
  registerForm = new FormGroup({
    UserName: new FormControl("", [Validators.required]),
    Password: new FormControl("", Validators.required),
    email: new FormControl("", [Validators.required, Validators.email]),
    ConfirmPassword:new FormControl("", Validators.required),
    RoleID:new FormControl(1)
})
// save(user){
//   this.service.register(user).subscribe((res) => {
//   })
// }
onSubmit() {
  this.service.register(this.registerForm.value).subscribe({  
  next: (response: any) => {
  this._sharedService.success("Success", "Register Successfully");
  this.router.navigate(['./login']);
  },
  
  error: (error) => {
    this._sharedService.error("error", error.Message);
  },
  
  });
}
}