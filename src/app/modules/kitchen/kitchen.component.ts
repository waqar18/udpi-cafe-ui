import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Menu } from 'app/Models/menu';
import { Order } from 'app/Models/order';
import { orderItem } from 'app/Models/orderitem';
import { kOrders } from 'app/Models/orders';
import { SharedService } from 'app/shared/services/shared.service';
import { environment } from 'environments/environment';
import { CountdownComponent } from 'ngx-countdown';
// import { number } from 'ngx-custom-validators/src/app/number/validator';

@Component({
  selector: 'app-kitchen',
  templateUrl: './kitchen.component.html',
  styleUrls: ['./kitchen.component.scss']
})
export class KitchenComponent implements OnInit {  
  isShown: boolean = false; // hidden by default
  HideOrder:boolean=true;
  OrederDetail: any = [];
  customerid;
  cartItemStatus = Array<orderItem>();
  AddToList = Array<Order>();
  menulist = Array<Menu>();
  kitchenDetailorder: kOrders;
  timeCounted;
  orderitem: orderItem
  preparation = 0;
  preparation2: any;
  currentTime: any;
  targetTime: any;
  eslapedTime:any;
  interval: any;
  baseURL;
  orderResponse:any
  selectedRowIndex;
  isDataLoaded = false;
  responeResult =[];
  constructor(private router: Router, public http: HttpClient, public shardService: SharedService) {
    this.kitchenDetailorder = new kOrders();
    this.orderitem = new orderItem();
    this.baseURL = environment.API_URL;
  }

  ngOnInit(): void {
    this.orderDetailAll();
  }

  //   ngAfterViewChecked()
  // {
  //   if(this.isDataLoaded == true)
  //   {
  //     this.cartItemStatus.forEach(element => {
  //       if(element.StatusID != 3)
  //       {
  //         var dataElement = document.getElementById(element.OrderItemID.toString());
  //         if(dataElement != null)
  //         {
  //           dataElement.removeAttribute('hidden');
  //         }
  //       }
  //     });
  //   }
  // }
  orderDetailAll() {
    this.http
      .get(
        environment.API_URL +
        "api/Category/GetAllOrders").subscribe((res: any) => {
          this.kitchenDetailorder = res;
        //   this.orderResponse=res
        //  this.responeResult.push(res);
          this.AddToList = this.kitchenDetailorder.order;
          this.cartItemStatus = this.kitchenDetailorder.orderItem;
          this.isDataLoaded=true;
           });
      }
  completeStatus(id: number,$CountDown) {
    this.orderitem.OrderItemID = id
    this.orderitem.StatusID = 3;
    $CountDown.stop();
    this.orderitem.CompletionTime = $CountDown.i.text;
    this.http.post(environment.API_URL + "api/Category/UpdateOrderItem", this.orderitem).subscribe((res: any) => {
    var orderID = this.cartItemStatus.find(item=>item.OrderItemID == id ).OrderID
    var index = this.cartItemStatus.findIndex(item=>item.OrderItemID == id )
    this.cartItemStatus.splice(index,1);
    var items = this.cartItemStatus.filter(item=>item.OrderID == orderID )
  if(items.length == 0){
  var index = this.AddToList.findIndex(item=>item.OrderID == orderID )
  this.AddToList.splice(index,1);
}


      this.shardService.success("Success", "Updated successfully");
    
        // document.getElementById('color').style.backgroundColor='green';
    })
       
  }

}


