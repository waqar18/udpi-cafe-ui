import { number } from 'ngx-custom-validators/src/app/number/validator';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Menu } from 'app/Models/menu';
import { environment } from 'environments/environment';
import { Order } from 'app/Models/order';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { SharedService } from 'app/shared/services/shared.service';
import { orderItem } from 'app/Models/orderitem';
import { element } from 'protractor';
import { Location } from '@angular/common';
import { ChangeDetectionStrategy } from '@angular/compiler/src/compiler_facade_interface';
import { PublicService } from '../public.service';
import { ApplicationSettings } from 'app/Models/applicationSettiings';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  @ViewChild('closebutton') closebutton;
  cartCountDetail: any;
  show: boolean = true;
  cartCountDetails: any;
  resume: Menu;
  resume2: Menu;
  baseURL;
  quantity: any;
  totalPrice = 0;
  cartMenu = [];
  menuItem = [];
  cartItem = [];
  RemainingCart = [];
  orderslist = [];
  cartItemHistory = [];
  customer: any;
  count = {};
  numberOfGuests: any;
  quantityCount = [];
  order: Order;
  qty = 0;
  page = 1;
  pageSize = 8;
  regularForm: FormGroup;
  submitted: false;
  totalCost: number;
  Discount: number;
  percenatgeAmount = 15;
  tip = 0;
  OrderTypeID: any;
  takeaway: any;
  Dine_in: any;
  delivery: any;
  Isshow: boolean = false;
  salesTax: 0;
  estimatedPrice = 0;
  quantities = 0;
  numberOfGuest = 0;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    public http: HttpClient,
    public _sharedService: SharedService,
    private _location: Location,
    private changeDetection: ChangeDetectorRef,
    private _publicService: PublicService
  ) {
    this.numberOfGuest = JSON.parse(localStorage.getItem('NumberOfGuests'));
    this.baseURL = environment.API_URL;
    for (let i = 1; i <= this.menuItem.length; i++) {
      this.menuItem.push({ Name: 'menuItem' + i });
    }
  }
  ngOnInit(): void {
    this.resume = new Menu();
    this.order = new Order();
    var Guests = localStorage.getItem('NumberOfGuests');
    this.delivery = localStorage.getItem('Delivery');
    this.OrderTypeID = parseInt(localStorage.getItem('ordertypeId'));
    this.Dine_in = this.delivery;
    this.takeaway = this.delivery;
    this.numberOfGuests = parseInt(Guests);
    this.cartItem = new Array();
    this.cartItem = JSON.parse(localStorage.getItem('cart'));
    this.getSalesTax();
    this.cartDetail();

  }
  getSalesTax() {
    this._publicService.getSalesTax().subscribe({
      next: (response: any) => {
        this.salesTax = response.Tax;
        this.cartDetail();
      },
      error: (error) => {
        console.log('error', error);
      },
    });
  }
  cartDetail() {
    this.totalPrice = 0;
    this.cartCountDetail = this.cartItem.length;
    for (let index = 0; index < this.cartItem.length; index++) {
      this.totalPrice +=
        parseFloat(this.cartItem[index].product.Price) *
        this.cartItem[index].product.Quantity;
    }
    this.percenatgeAmount = this.numberOfGuests > 4 ? 15 : 0;
    var Amount = (this.totalPrice / 100) * this.percenatgeAmount;
    console.log('Total Price', this.totalPrice);
    console.log('Amount', Amount);
    var saletax = (this.totalPrice / 100) * this.salesTax;
    console.log('saletax', saletax);
    var totalGuests = this.numberOfGuests;
    this.estimatedPrice = this.totalPrice + Amount + saletax;
    this.estimatedPrice =
      this.estimatedPrice + (this.tip != undefined ? this.tip : 0);
  }
  checkOut(cartItem) {}
  FormSubmit() {
    this.order.RestaurantID = 1;
    this.order.OrderTypeID = this.OrderTypeID;
    this.order.LocationID = 1;
    this.order.StatusID = 1;
    this.order.Gratuity = this.percenatgeAmount;
    this.order.Tip = this.tip;
    var PreparationTime = 0;
    this.order.orderItem = [];
    for (let index = 0; index < this.cartItem.length; index++) {
      let orderItemObj: any = {
        MenuID: this.cartItem[index].product.MenuID,
        Quantity: this.cartItem[index].product.Quantity,
        MenuName: this.cartItem[index].product.MenuName,
        Price: this.cartItem[index].product.Price,
        OriginalPrice: this.cartItem[index].product.OriginalPrice,
        Notes: this.cartItem[index].Notes,
      };
      this.order.orderItem.push(orderItemObj);
      PreparationTime += this.cartItem[index].product.PreparationTime;
    }
    this.order.PreparationTime = PreparationTime;
    this.order.TotalAmount = this.estimatedPrice;
    this.order.PromoCode = '12%';
    this.percenatgeAmount = parseInt(this.order.PromoCode);
    this.http
      .post(environment.API_URL + 'api/Category/SaveOrder', this.order)
      .subscribe((response) => {
        this.cartItemHistory = JSON.parse(localStorage.getItem('cart'));
        var count =
          this._sharedService.customer != undefined
            ? this._sharedService.customer.orderCount
            : 0;
        localStorage.clear();
        this._sharedService.customer = {
          firstname: this.order.CustomerFirstName,
          lastname: this.order.CustomerLastName,
          mobile: this.order.MobilePhone,
          PreparationTime: this.order.PreparationTime,
          customerId: response,
          orderCount: count + 1,
        };
        this._sharedService.changeCart.next();
        sessionStorage.setItem(
          'customer',
          JSON.stringify(this._sharedService.customer)
        );
        this.closebutton.nativeElement.click();
        this.router.navigate([
          'order-dispatch/',
          this._sharedService.customer.mobile,
        ]);
      });
  }
  increment(item) {
    item = item;
    let shopping_cart;
    shopping_cart = JSON.parse(localStorage.getItem('cart'));
    for (let i in shopping_cart) {
      if (item.product.MenuID == shopping_cart[i].product.MenuID) {
        shopping_cart[i].product.Quantity += 1;
        var index = this.cartItem.findIndex(
          (item) => item.product.MenuID == shopping_cart[i].product.MenuID
        );
        // item = null;
        if (index > -1) {
          this.cartItem[index].product.Quantity =
            shopping_cart[i].product.Quantity;
        }
        break;
      }
    }

    localStorage.setItem('cart', JSON.stringify(shopping_cart));
    this.cartDetail();
  }
  decrement(item) {
    if (item.product.Quantity >= 2) {
      item = item;
      let shopping_cart;
      shopping_cart = JSON.parse(localStorage.getItem('cart'));
      for (let i in shopping_cart) {
        if (item.product.MenuID == shopping_cart[i].product.MenuID) {
          shopping_cart[i].product.Quantity -= 1;
          var index = this.cartItem.findIndex(
            (item) => item.product.MenuID == shopping_cart[i].product.MenuID
          );
          // item = null;
          if (index > -1) {
            this.cartItem[index].product.Quantity =
              shopping_cart[i].product.Quantity;
          }
          // item = null;
          break;
        }
      }
      localStorage.setItem('cart', JSON.stringify(shopping_cart));
      this.cartDetail();
    } else {
    }
  }
  removeItem(index) {
    var existingItems = JSON.parse(localStorage.getItem('cart'));
    existingItems.splice(index, 1);
    this.cartItem = existingItems;
    localStorage.setItem('cart', JSON.stringify(existingItems));
    this.cartItem = existingItems;
    this.cartDetail();
    if (this.cartItem.length == 0) {
      this.router.navigate(['/']);
    }
    this._sharedService.changeCart.next();
  }
  back() {
    this.router.navigate(['/']);
  }
  prevPage(){
    this._location.back();
  }
}
