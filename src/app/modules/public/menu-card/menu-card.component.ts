import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from 'app/shared/services/category.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-menu-card',
  templateUrl: './menu-card.component.html',
  styleUrls: ['./menu-card.component.scss'],
})
export class MenuCardComponent implements OnInit {
  category: any;
  id: string;
  baseURL;
  constructor(
    private router: Router,
    private Category: CategoryService,
    private route: ActivatedRoute
  ) {
    this.baseURL = environment.API_URL;
  }

  ngOnInit(): void {
    this.Category.GetCategory().subscribe((data) => {
      this.category = data;
    });
  }
  onView(CategoryID: number){
    this.router.navigate(['menu-category/' + CategoryID]);
  }
}
