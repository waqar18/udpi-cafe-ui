import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCataComponent } from './menu-cata.component';

describe('MenuCataComponent', () => {
  let component: MenuCataComponent;
  let fixture: ComponentFixture<MenuCataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuCataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
