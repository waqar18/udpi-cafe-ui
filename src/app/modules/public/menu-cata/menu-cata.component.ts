import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Type } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from 'app/shared/services/shared.service';
import { environment } from 'environments/environment';
import { Location } from '@angular/common';

@Component({
  selector: 'app-menu-cata',
  templateUrl: './menu-cata.component.html',
  styleUrls: ['./menu-cata.component.scss'],
})
export class MenuCataComponent implements OnInit {
  id: string;
  menu: any;
  sectionObj: any;
  tmpSectionObj: any;
  baseURL;
  show: boolean = true;
  hide: boolean = false;
  items;
  MenuItem: any = [];
  Menulength: any;
  constructor(
    private router: Router,
    public http: HttpClient,
    private route: ActivatedRoute,
    public _sharedService: SharedService,
    private _location: Location
  ) {
    this.baseURL = environment.API_URL;
  }
  ngOnInit(): void {
    this.http
      .get(
        environment.API_URL +
          '/api/Category/GetMenuByCategory/' +
          this.route.snapshot.params.id
      )
      .subscribe((res: any) => {
        this.menu = res;
        this.MenuItem = this.menu.length
      });
  }
  addToCart(item,item2) {

     item.Quantity = 1;

    let local_storage;
    let itemsInCart = [];
    this.items = {
      product: item,
      quantity: 1,
    };
    if (localStorage.getItem('cart') == null) {
      local_storage = [];

      itemsInCart.push(this.items);
      this._sharedService.success('', 'Added successfully');
      localStorage.setItem('cart', JSON.stringify(itemsInCart));
    } else {
      local_storage = JSON.parse(localStorage.getItem('cart'));
      for (var i in local_storage) {
        if (this.items.product.MenuID == local_storage[i].product.MenuID) {
          local_storage[i].product.Quantity += 1;
          this.items = null;
          break;
        }
      }
      if (this.items) {
        itemsInCart.push(this.items);
      }
      local_storage.forEach(function (item) {
        itemsInCart.push(item);
      });
      localStorage.setItem('cart', JSON.stringify(itemsInCart));
      this._sharedService.success('', 'Added to cart');
      this._sharedService.toastr;
      //  this.show =false;
    }
    this._sharedService.changeCart.next();
  }
  prevPage(){
    this._location.back();
  }
}
