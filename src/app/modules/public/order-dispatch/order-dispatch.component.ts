import { Order } from './../../../Models/order';
import { CustomHttpService } from './../../../shared/services/custom-http.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { orderItem } from 'app/Models/orderitem';
import { SharedService } from 'app/shared/services/shared.service';
import { environment } from 'environments/environment';
@Component({
  selector: 'app-order-dispatch',
  templateUrl: './order-dispatch.component.html',
  styleUrls: ['./order-dispatch.component.scss'],
})
export class OrderDispatchComponent implements OnInit {
  customerOrederDetail;
  cartItemHistory = [];
  totalPrice = 0;
  cartCountDetail;
  baseURL;
  value2: number;
  value: number;
  minutesInSecond;
  customerOreder;
  countDown: any;
  OrederDetail: any = [];
  OrderContact;
  orderPercent = 20;
  dispatchList: Order;
  OrderItemResponse: [];
  OrderResponse: [];
  cartItemStatus = Array<orderItem>();
  filtered: any;
  orderLength = 0;
  OrderID: any;
  Gratuity: any;
  Tip: any;
  TotalAmount: any;
  orderItemList: any;
  AddToList: any;
  constructor(
    public shardService: SharedService,
    private router: Router,
    public http: HttpClient,
    private _customHttpService: CustomHttpService,
    public _sharedService: SharedService
  ) {
    this.baseURL = environment.API_URL;
    this.dispatchList = new Order();
  }
  ngOnInit(): void {
    // this.customerDetail();
    // this.counterHistroy();
    this.OrderReceive();
    // this.cart();
    setInterval(() => {
      this.orderPercent = this.orderPercent + 5;
    }, 10000);
    this._sharedService.changeOrderLenght.next();
  }
  customerDetail() {
    this.customerOrederDetail = JSON.parse(
      sessionStorage.getItem('customerOrder')
    );
    this.value = this.shardService.count;
  }
  counterHistroy() {
    this.customerOreder = JSON.parse(sessionStorage.getItem('counter'));
    this.value2 = this.customerOrederDetail.counter;
  }
  cart() {
    this.cartItemHistory = JSON.parse(sessionStorage.getItem('cart'));
    this.cartCountDetail = this.cartItemHistory.length;
    for (let index = 0; index < this.cartItemHistory.length; index++) {
      this.totalPrice +=
        parseFloat(this.cartItemHistory[index].product.Price) *
        this.cartItemHistory[index].product.Quantity;
    }
  }
  OrderReceive() {
    // var mobileNumber = JSON.parse(sessionStorage.getItem('customerOrder'));
    // this.OrderContact = mobileNumber;
    // this._customHttpService.getWithoutHeader()
    // var data = { MobilePhone: this.OrderContact };
    this.http
      .get(environment.API_URL + 'api/Category/GetOrderDetail?MobilePhone=' + this._sharedService.customer.mobile)
      .subscribe((res: any) => {
        this.orderItemList = res.OrderItem;
        this.AddToList = res.Orders;
        this._sharedService.OrdersDetail = true;
        this._sharedService.customer.orderCount = this.AddToList.length
        sessionStorage.setItem('customer', JSON.stringify(this._sharedService.customer))
      });
  }
  placeAnotherOrder() {
    this.router.navigate(['/']);
  }
  orderItemAgainstOrderID(orderID: any, Gratuity: any, tip: any, TotalAmount: any) {
    this.filtered = this.orderItemList.filter(orders => orders.OrderID === orderID);
    this.orderLength = orderID;
    this.Gratuity = Gratuity;
    this.Tip = tip;
    this.OrderID = orderID;
    this.TotalAmount = TotalAmount;
  }
  scroll() {
    var el = document.getElementById('order');
    el.scrollIntoView({
      behavior: 'smooth'
    });
  }
  receiveLength($event) {
    this.orderLength = $event;
  }
}
