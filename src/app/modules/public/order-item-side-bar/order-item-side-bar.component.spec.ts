import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderItemSideBarComponent } from './order-item-side-bar.component';

describe('OrderItemSideBarComponent', () => {
  let component: OrderItemSideBarComponent;
  let fixture: ComponentFixture<OrderItemSideBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderItemSideBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderItemSideBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
