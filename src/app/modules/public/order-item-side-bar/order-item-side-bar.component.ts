import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-order-item-side-bar',
  templateUrl: './order-item-side-bar.component.html',
  styleUrls: ['./order-item-side-bar.component.scss']
})
export class OrderItemSideBarComponent implements OnInit {
  @Input() orderItem: any;
  @Input() orderID: any;
  @Input() gratuity: any;
  @Input() tip: any;
  @Input() totalAmount: any;
  base_URL = environment.API_URL;
  length = 0;
  @Output() Orderlength = new EventEmitter<number>();
  constructor() { }

  ngOnInit(): void {
  }
  hideSideBar() {
    this.Orderlength.emit(this.length);
  }
}
