import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KitchenComponent } from '../kitchen/kitchen.component';
import { CartComponent } from './cart/cart.component';
import { MenuCardComponent } from './menu-card/menu-card.component';
import { MenuCataComponent } from './menu-cata/menu-cata.component';
import { MenuHomeComponent } from './menu-home/menu-home.component';
import { MenuComponent } from './menu/menu.component';
import { OrderDispatchComponent } from './order-dispatch/order-dispatch.component';
import { PublicComponent } from './public/public.component';



const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: 'menu',
        component: MenuComponent
      },
      {
        path: 'menu-home',
        component: MenuHomeComponent
      },
      {
        path: 'menu-category/:id',
        component: MenuCataComponent
      },
      {
        path: 'order-list',
        component: CartComponent
      },
      {
        path: 'order-dispatch/:id',
        component: OrderDispatchComponent
      },
      {
        path: '',
        component: MenuCardComponent
      },
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
      }
      // {
      //   path:'order-status',
      //   component:KitchenComponent
      // },
      // {
      //   path:'order-delete/:id',
      //   component:KitchenComponent
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
