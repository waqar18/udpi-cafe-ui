import { SharedModule } from 'app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public/public.component';
import { MenuComponent } from './menu/menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuHomeComponent } from './menu-home/menu-home.component';
import { MenuCardComponent } from './menu-card/menu-card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MenuCataComponent } from './menu-cata/menu-cata.component';
import { CartComponent } from './cart/cart.component';
import { OrderDispatchComponent } from './order-dispatch/order-dispatch.component';
import { NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CountdownModule } from 'ngx-countdown';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { createTranslateLoader } from 'app/app.module';
import { OrderItemSideBarComponent } from './order-item-side-bar/order-item-side-bar.component';



@NgModule({
  declarations: [PublicComponent,
     MenuComponent, MenuHomeComponent, MenuCardComponent, MenuCataComponent, CartComponent, OrderDispatchComponent, OrderItemSideBarComponent],
  imports: [
    CommonModule,
    PublicRoutingModule,
    CountdownModule,
    NgbModule,
    FormsModule,
    SharedModule,
    NgCircleProgressModule.forRoot({
      "backgroundColor": "#FDB900",
      "radius": 100,
      "maxPercent": 100,
      "units": " % Order",
      "unitsColor": "#483500",
      "outerStrokeWidth": 5,
      "outerStrokeColor": "#FFFFFF",
      "innerStrokeColor": "#FFFFFF",
      "titleColor": "#483500",
      "subtitleColor": "#483500",
      "showSubtitle": false,
      "showInnerStroke": false,
      "startFromZero": false
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
     ReactiveFormsModule 

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class PublicModule { }
