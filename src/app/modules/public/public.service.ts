import { CustomHttpService } from 'app/shared/services/custom-http.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private _customHttpService: CustomHttpService) {
   }
   getSalesTax() {
      return this._customHttpService.get('api/Category/GetSalesTax');
  }
}
