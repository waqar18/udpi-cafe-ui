import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'app/Models/user';
@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.scss']
})
export class PublicComponent implements OnInit {
user: User;
constructor(private router: Router) {
this.user = new User();
}
ngOnInit(): void {
}
// tslint:disable-next-line: typedef
login() {
// this.router.navigate(['./public']);
}
guest(){
  this.router.navigate(['./menu-list'])
}
}

