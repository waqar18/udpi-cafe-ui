import swal from 'sweetalert2';
import { Router } from '@angular/router';
// Chaining modals / Steps
export class sweetalert2 {
constructor(public router: Router) {
  }
  steps() {
    swal.queue([ {  
        title: 'WELCOME TO UDIPICAFE  ',
        showCancelButton: true,
         cancelButtonColor: '#FF586B',
        confirmButtonText: 'Dine-in',
        cancelButtonText: 'Take Out',
        customClass: {
            confirmButton: 'btn btn-success btn-raised mr-5',
            cancelButton: 'btn btn-danger btn-raised'
          },
        buttonsStyling: false,
         inputValidator: function(result) {
         return new Promise(function(resolve, reject) {
      if (result) {
      } else {
        reject('You need to select something!');
        alert('You need to select something!')
      }
    }); 
}    
        },
        {
            title: 'Number of Guests ',
            input: 'text',
            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
          }
        
    ]).then(function () {
        swal.fire({
            title: 'All done!',
            text: 'Great job :)',
            confirmButtonText: 'Lovely!',
            showCancelButton: false,
            
        });
        this.router.navigate = ['/']
  
    })
    
}
}
 
