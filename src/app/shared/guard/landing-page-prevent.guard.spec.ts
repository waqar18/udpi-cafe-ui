import { TestBed } from '@angular/core/testing';

import { LandingPagePreventGuard } from './landing-page-prevent.guard';

describe('LandingPagePreventGuard', () => {
  let guard: LandingPagePreventGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(LandingPagePreventGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
