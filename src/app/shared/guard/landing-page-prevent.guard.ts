import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LandingPagePreventGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate(): boolean {
    
    const userRole = localStorage.getItem('RoleID');
    // tslint:disable-next-line: prefer-const
    // tslint:disable-next-line: radix
    const roleId = parseInt(userRole);
    if (roleId != 1) {
      if (roleId != 2) {
        return true; 
      }
    }
        // not logged in so redirect to login page with the return url
    this.router.navigate(['/admin']);
    return false;
      }
  
}
