import { Component, Output, EventEmitter, OnInit, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { LayoutService } from '../services/layout.service';
import { ConfigService } from '../services/config.service';
import { number } from 'ngx-custom-validators/src/app/number/validator';
import { SharedService } from '../services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent implements OnInit, AfterViewInit {
  currentLang = "en";
  toggleClass = "ft-maximize";
  placement = "bottom-right";
  public isCollapsed = true;
  cartCount = 0 ;
  currentUser:string
  cartItems:[];
  ordersCount = 0;
  token: string;
  @Output()
  toggleHideSidebar = new EventEmitter<Object>();
  public config: any = {};
  constructor(public translate: TranslateService, 
    private layoutService: LayoutService, 
    private configService: ConfigService,
    public _sharedService: SharedService,
    public router: Router) {
      this.currentUser = localStorage.getItem('name');
    const browserLang: string = translate.getBrowserLang();
    translate.use(browserLang.match(/en|es|pt|de/) ? browserLang : "en");
    this._sharedService.changeCart.subscribe((res: any) =>{
      this.count();
      this.orderCount();
   })
  }

  ngOnInit() {
    this.config = this.configService.templateConf;
     this.count();
     this.orderCount();


    this.token =   localStorage.getItem('accesstoken');
  }
  ngAfterViewInit() {
    if(this.config.layout.dir) {
      const dir = this.config.layout.dir;
        if (dir === "rtl") {
          this.placement = "bottom-left";
        } else if (dir === "ltr") {
          this.placement = "bottom-right";
        }
    }
  }
  ChangeLanguage(language: string) {
    this.translate.use(language);
  }

  ToggleClass() {
    if (this.toggleClass === "ft-maximize") {
      this.toggleClass = "ft-minimize";
    } else {
      this.toggleClass = "ft-maximize";
    }
  }

  toggleNotificationSidebar() {
    this.layoutService.emitChange(true);
  }

  toggleSidebar() {
    const appSidebar = document.getElementsByClassName("app-sidebar")[0];
    if (appSidebar.classList.contains("hide-sidebar")) {
      this.toggleHideSidebar.emit(false);
    } else {
      this.toggleHideSidebar.emit(true);
    }
  }
  count(){
var tmpdata = JSON.parse(localStorage.getItem('cart'));
if(tmpdata != null){
  this.cartCount= tmpdata.length;
  this.currentUser = localStorage.getItem('name');
}
else{
  this.cartCount = 0;
  this.currentUser = localStorage.getItem('name');
}

  }
  Addtocart(){
    this.cartItems = this._sharedService.product;
    // localStorage.setItem('resume',JSON.stringify(this.cartItems))
  }
  isViewNavbar() {
    if (this.router.url.includes('/login') || this.router.url.includes('/register') || this.router.url.includes('/reset')) {
      return true;
    }
  }
  steps(){
  this._sharedService.steps();
  }

  orderCount() {
    var tmpdata = JSON.parse(sessionStorage.getItem('OrderLenght'));
if (tmpdata != null) {
  this.ordersCount = tmpdata;
  // this.currentUser = localStorage.getItem('name');
}
else {
  this.ordersCount = 0;
  // this.currentUser = localStorage.getItem('name');
}
  }

  onNavigateToOrders() {
    this.router.navigate(['order-dispatch/' , this._sharedService.customer.mobile]);
  }
  logOut() {
    localStorage.removeItem('name');
    localStorage.removeItem('accesstoken');
    localStorage.removeItem('RoleID');
    this._sharedService.logedName = undefined;
    this.router.navigate (['/login']);
  }
}
