import { Pipe, PipeTransform } from '@angular/core';
import { Order } from 'app/Models/order';

@Pipe({
  name: 'orders',
  pure:false
})
export class OrdersPipe implements PipeTransform {
  transform(value: any, filterValue: any, orderItem: string) {
    if (value == undefined || (filterValue == undefined || filterValue == '' && filterValue != 0) || (value != undefined && value.length == 0)) {
      return value;
    }
    return value.filter(item => item[orderItem] == filterValue);
  }  

}
