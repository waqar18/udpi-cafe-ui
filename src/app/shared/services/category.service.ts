import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomHttpService } from './custom-http.service';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  public baseUrl = environment.API_URL;
  url= 'api/Category/deleteCategory'
  constructor(private _http: HttpClient,public _customhttp:CustomHttpService) { }
  
  GetCategory(): Observable<any> {
    return this._http.get(this.baseUrl + "api/Category/GetAllCategories");
  }
  GetMenu(): Observable<any> {
    return this._http.get(this.baseUrl + "api/Category/GetMenuByCategory");
  }
  GetAllMenu(): Observable<any> {
    return this._http.get(this.baseUrl + "api/Category/GetAllMenu");
  }
  deleteCategory(id){ 
    const url = 'api/Category/deleteCategory';
    return this._customhttp.delete(url, {category:id});
    
    
  }
  updateCategory(obj){ 
    return this._customhttp.post( 'api/Category/updateCategory',obj); 
  }
  deleteMenu(id){ 
    return this._customhttp.delete('api/Category/deleteMenu',{menu:id}); 
  }
  updateMenu(obj){ 
    return this._customhttp.post( 'api/Category/updateMenu',obj); 
  }
  getMenu(obj){ 
    return this._customhttp.post( 'api/Category/GetMenuByID',obj); 
  }
  
}
