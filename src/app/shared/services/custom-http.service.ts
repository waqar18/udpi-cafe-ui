import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { SharedService } from './shared.service';
import { environment } from '../../../../src/environments/environment';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
// tslint:disable-next-line: import-blacklist
import 'rxjs/Rx';
import { User } from './../../Models/user';
import { catchError, map } from 'rxjs/operators';
@Injectable({
providedIn: 'root'
})
export class CustomHttpService {
public baseUrl = environment.API_URL;
public pendingRequests = 0;
public httpOptions;
public currentUser: Observable<User>;
// tslint:disable-next-line: variable-name
constructor(private _http: HttpClient,
// tslint:disable-next-line: variable-name
public _sharedService: SharedService) { }

get(url: string, data?: any): Observable<any[]> {
this.pendingRequests++;
this._sharedService.loading = true;
this.httpOptions = {
headers: new HttpHeaders({
'Content-Type': 'application/json',
Authorization: 'Bearer ' + localStorage.getItem('token'),
}),
params: data
};
return this._http.get(this.baseUrl + url, this.httpOptions)
.map((response: any) => {
this.pendingRequests--;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
this._sharedService.loading = false;
}
return response;
})
.catch(e => {
this.pendingRequests--;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
}
this._sharedService.loading = false;
if (e.status === 401) {

this.signout();
}
return throwError(e.error);
});
}

delete(url: string, data?: any): Observable<any[]> {
this.pendingRequests++;
this._sharedService.loading = true;
this.httpOptions = {
headers: new HttpHeaders({
'Content-Type': 'application/json',

Authorization: 'Bearer ' + localStorage.getItem('accesstoken'),
}),
params: data
};
return this._http.delete(this.baseUrl + url, this.httpOptions)
.map((response: any) => {
this.pendingRequests--;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
this._sharedService.loading = false;
}
return response;
})
.catch(e => {
this.pendingRequests--;
this._sharedService.loading = false;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
}
if (e.status === 401) {

this.signout();
}
return throwError(e.error);
});
}

getWithoutHeader(url: string, data?: any): Observable<any[]> {
this.pendingRequests++;
this._sharedService.loading = true;
this.httpOptions = {
headers: new HttpHeaders({
'Content-Type': 'application/json',
Authorization: 'Bearer ' + localStorage.getItem('access_token'),
})
};
return this._http.get(this.baseUrl + url, this.httpOptions)
.map((response: any) => {
this.pendingRequests--;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
this._sharedService.loading = false;
}
return response;
})
.catch(e => {
this.pendingRequests--;
this._sharedService.loading = false;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
}

if (e.status === 401) {

this.signout();
}
return throwError(e.error);
});
}


postWithoutHeader(url: string, data: any): Observable<any[]> {
this.pendingRequests++;
this._sharedService.loading = true;
this.httpOptions = {
headers: new HttpHeaders({
'Content-Type': 'application/json'
})
};
return this._http.post(this.baseUrl + url, data, this.httpOptions)
.map((response: any) => {
this.pendingRequests--;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
this._sharedService.loading = false;
}
return response;
})
.catch(e => {
this.pendingRequests--;
this._sharedService.loading = false;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
}
if (e.status === 401) {

this.signout();
}
// return Observable.throw(e);
return throwError(e.error);
});
}

post(url: string, data: any): Observable<any[]> {
this.pendingRequests++;
this._sharedService.loading = true;
this.httpOptions = {
headers: new HttpHeaders({
'Content-Type': 'application/json',
Authorization: 'Bearer ' + localStorage.getItem('access_token'),
})
};
return this._http.post(this.baseUrl + url, data, this.httpOptions)
.map((response: any) => {
this.pendingRequests--;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
this._sharedService.loading = false;
}
return response;
})
.catch(e => {
this.pendingRequests--;
this._sharedService.loading = false;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
}
if (e.status === 401) {

this.signout();
}
return throwError(e.error);
});
}


resetPassword(url: string, data: any): Observable<any[]> {
this.pendingRequests++;
this._sharedService.loading = true;
this.httpOptions = {
headers: new HttpHeaders({
'Content-Type': 'application/json',
// tslint:disable-next-line: quotemark
Authorization: "Bearer " + localStorage.getItem("ResetToken"),
})
};
return this._http.post(this.baseUrl + url, data, this.httpOptions)
.map((response: any) => {
this.pendingRequests--;
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
this._sharedService.loading = false;
}
return response;
})
.catch(e => {
this.pendingRequests--;
this._sharedService.loading = false;
// tslint:disable-next-line: triple-equals
// tslint:disable-next-line: triple-equals
if (this.pendingRequests == 0) {
}
if (e.status === 401) {

this.signout();
}
return throwError(e.error);
});
}

// tslint:disable-next-line: typedef
signout() {
localStorage.clear();
// this._router.navigate(['/auth/sign-in'])
}

signin(data: any) {
    this.pendingRequests++;
    this.httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json',
    }),
    };
    return this._http
    .post(
    this.baseUrl + 'token',
    data
    )
    .pipe(
    map((response: any) => {
    this.pendingRequests--;
    if (this.pendingRequests <= 0) {
    this.pendingRequests = 0;
    }
    return response;
    }),
    catchError((e) => {
    this.pendingRequests--;
    if (this.pendingRequests <= 0) {
    this.pendingRequests = 0;
    }
    if (e.status === 401) {
    this.signout();
    }
    return throwError(e.error);
    })
    );
    }
}
