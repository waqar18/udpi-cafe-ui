import { Injectable, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Category } from 'app/Models/category';
import { Menu } from 'app/Models/menu';
import { isThisMonth, isThursday } from 'date-fns';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { queue } from 'rxjs/internal/scheduler/queue';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  numberofGuests: number;
  orderlenght: any;
  customer: any;
  OrdersDetail = false;
  changeCart: Subject<any>;
  changeQuantity: Subject<any>;
  changeOrderLenght: Subject<any>;
  product:any;
  count:any;
  value:any;
  result:any
  NumberOfGuests:any
loading:boolean
isDeliverySelected:boolean = false;
isDenied:boolean = false;
updateCategory:Category;
updateMenu:Menu
 menuItems: any[];
 logedName: string;
 deliveryType:string = '';
 roleId: number;
  selected_role_id: number;
  constructor(public toastr: ToastrService , private router : Router) {
    this.changeCart = new Subject<any>();
    this.changeQuantity = new Subject<any>();
    this.changeOrderLenght = new Subject<any>();
    if (sessionStorage.getItem('customer') != undefined && sessionStorage.getItem('customer')!= '' ) {
    this.customer = JSON.parse(sessionStorage.getItem('customer'));
    }
    if (localStorage.getItem('Delivery') != undefined && localStorage.getItem('Delivery')!= '' ) {
      this.deliveryType = localStorage.getItem('Delivery');
      }
    if (localStorage.getItem('accesstoken') != undefined && localStorage.getItem('accesstoken')!= '' ) {
        this.logedName = localStorage.getItem('accesstoken');
     }
     if (localStorage.getItem('RoleID') != undefined && localStorage.getItem('RoleID')!= '' ) {
      this.roleId = parseInt(localStorage.getItem('RoleID'));
   }
   }
   addCart(product) {
    this.product = product
 }
 // notification service is injected and used here at one place
 // instead of injecting in every component this will make it reusable and clean code
 // for success
  success(title: string, Message: string) {
    this.toastr.success(
      title,
      Message,
      {
        positionClass:'toast-top-center',
        progressBar: false,
        // timeOut: 5000,
      }
    )
  }
  successPriceUpdate(title: string, Message: string) {
    this.toastr.success(
      title,
      Message,
      {
        // positionClass:'toast-top-center',
        progressBar: false,
        // timeOut: 5000,
      }
    )
  }
  warringPriceUpdate(title: string, Message: string) {
    this.toastr.warning(
      title,
      Message,
      {
        progressBar: false,
      }
    )
  }
  //for error
  error(title: string, Message: string) {
    this.toastr.error(
      title,
      Message,
      {
        timeOut: 5000,
      }
    )
  }
  countDown(countDown: any){
    this.count = countDown;
  }
//   steps() {
// Swal.mixin({
//   confirmButtonText: 'Next &rarr;',
//   allowOutsideClick: false

// }).queue([
//   {
//     title: 'WELCOME TO UDIPICAFE  ',
//     icon: 'info',
//       showCancelButton: true,
//        cancelButtonColor: '#FF586B',
//       confirmButtonText: 'Dine-in',
//       cancelButtonText: 'Take Out',
//       customClass: {
//           confirmButton: 'btn btn-success btn-raised mr-5',
//           cancelButton: 'btn btn-danger btn-raised'
//         },
//         preConfirm: (res) => {
//           this.isDeliverySelected = true;
//           this.saveResultToLocalStorage(res);
//          }
//   },
//   {
//               icon: 'question',
//               title: 'Number of Guests ',
//               input: 'text',
//               showCancelButton: true,
//               confirmButtonText: 'Continue &rarr;',
//               inputValidator: (value) => {
//                 if (!value) {
//                   return 'Enter Number of Guests!'
//                 }
//                }
//   },
  
// ]).then((result : any) => {
//   if(result.dismiss) {
//   //  this.NumberOfGuests = localStorage.getItem('NumberOfGuests');
//   //  this.NumberOfGuests = 1;
//     this.saveResultToLocalStorage('cancel');
//     Swal.fire({
//       title: 'All done!',
//         text: 'Great job :)',
//         confirmButtonText: 'Continue As Guest!',
//         showCancelButton: false, 
// })
// this.router.navigate (['/order-list'])
//   }
//   var res =[];
//   if(!result.dismiss){
//     res.push(result)
//   }
//   if (res) {
//     if(res==null){
//       localStorage.setItem('NumberOfGuests','1')
//     }
//     else if(res)
//      localStorage.setItem('NumberOfGuests',res[0].value[1])
//     Swal.fire({
//           title: 'All done!',
//             text: 'Great job :)',
//             confirmButtonText: 'Continue As Guest!',
//             showCancelButton: false, 
//     })
//   }
//       this.router.navigate (['order-list'])
//       // window.location.reload();
//        }) 
// }
// saveResultToLocalStorage(data){
//   if(data===true){
//     localStorage.setItem('Delivery','dinein' )
//     this.deliveryType= 'dinein'

//   } else if(data ==='cancel') {
//    var numberOfGuest = localStorage.getItem('NumberOfGuests');
//     localStorage.setItem('Delivery','takeout' )
//     this.deliveryType= 'takeout'

//   } else {
//     localStorage.setItem('Delivery','' )
//   }
// }
steps() {
  Swal.mixin({
    confirmButtonText: 'Next &rarr;',
    allowOutsideClick: false
  }).queue([
    {
      title: 'WELCOME TO UDIPICAFE  ',
      icon: 'info',
        showCloseButton: true,
        showCancelButton: true,
         cancelButtonColor: '#FF586B',
        confirmButtonText: 'Dine –In',
        cancelButtonText: 'Take Out',
        customClass: {
            confirmButton: 'btn btn-success btn-raised mr-5',
            cancelButton: 'btn btn-danger btn-raised'
          },
          preConfirm: (res) => {
            this.isDenied = true;
            this.saveResultToLocalStorage(res);
           }
    },
    {
                icon: 'question',
                title: 'Number of Guests ',
                input: 'text',
                showCloseButton: true,
                showCancelButton: true,
                showDenyButton:false,
                denyButtonText:"No",
                confirmButtonText: 'Continue &rarr;',
                
                inputValidator: (value) => {
                  if (!value) {
                    return 'Enter Number of Guests!'
                  }
                 },
                 preDeny: (result) => {
                 if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')
                  }
                }
    },
    
  ]).then((result : any) => {
    var deliveryDine =localStorage.getItem('Delivery')
    if(result.dismiss == 'cancel' &&  deliveryDine=="dinein" ){
      // this.router.navigate(['/']);
      localStorage.removeItem('Delivery');
    }
     if(result.dismiss=="close"){
      // this.router.navigate(['/']);
    }
    else{
    if(result.dismiss) {
    //  this.NumberOfGuests = localStorage.getItem('NumberOfGuests');
    //  this.NumberOfGuests = 1;
      this.saveResultToLocalStorage('cancel');
      Swal.fire({
        title: 'All done!',
          text: 'Great job :)',
          confirmButtonText: 'Continue As Guest!',
          showCancelButton: false, 
          showCloseButton: true,
          
  })
  this.router.navigate (['/order-list'])
    }
    var res =[];
    if(!result.dismiss){
      res.push(result)
    }
    if (res) {
        localStorage.setItem('NumberOfGuests',res[0]?.value[1] || null)
        this.numberofGuests = res[0]?.value[1];   
      Swal.fire({
            title: 'All done!',
              text: 'Great job :)',
              confirmButtonText: 'Continue As Guest!',
              showCancelButton: false, 
              showCloseButton: true,
      })
      if(result.dismiss == 'cancel' &&  deliveryDine=="dinein" ){
        this.router.navigate(['/']);
      }
      
      else{
        if(result.dismiss == 'cancel' &&  deliveryDine=="takeout" ){
          this.router.navigate (['/order-list'])
        }
        if(result.dismiss == undefined &&  deliveryDine=="dinein" ){
          this.router.navigate (['/order-list'])
        }

      }
      
    }
       
  }
         }) 
  
        }

  saveResultToLocalStorage(data){
    if(data===true){
      localStorage.setItem('Delivery','dinein' )
      this.deliveryType= 'dinein',
      localStorage.setItem('ordertypeId','1')
  
    } else if(data ==='cancel') {
      localStorage.setItem('Delivery','takeout' )
      this.deliveryType= 'takeout'
      localStorage.setItem('ordertypeId','2')
    } else {
      localStorage.setItem('Delivery','' )
    }
  }
  Isdeniedrouter(data){
    this.router.navigate (['/'])
  }
}