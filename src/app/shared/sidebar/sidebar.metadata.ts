
// Sidebar route metadata
export interface RouteInfo {
    roles: any;
    path: string;
    title: string;
    icon: string;
    class: string;
    badge: string;
    badgeClass: string;
    isExternalLink: boolean;
    submenu : RouteInfo[];
    canActivate:any;
}
